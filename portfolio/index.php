<!doctype html>
<html>
	<head>
    	<meta charset="utf-8">
    	<meta name="viewport" content="width=device-width,initial-scale=1">
      <link rel="stylesheet" href="_reset.css">
      <link href="https://fonts.googleapis.com/css?family=Diplomata|Oswald:400,700|Roboto+Slab:400,700&amp;subset=latin-ext" rel="stylesheet">
      <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
     
      
    <title>VCS</title>  
      <link rel="stylesheet" type="text/css" href="slick.css"/>
      <link rel="stylesheet" type="text/css" href="slick-theme.css"/>
      <link rel="stylesheet" href="custom.css">
     
  	</head>

  	<body>

  		<header>
        <div class="center centerplus">
    			<ul class="meniu">  				
    				<li><a href="#aboutme">About Me</a></li>    				
    				<li class="has-dropdown">
              <a href="#portfolio">Portfolio</a>
              <ul class="dropdown-menu">
                <li><a href="https://bitbucket.org/product" target="_blank">GALLERIES ARE FUNNY</a></li>
                <li><a href="https://bitbucket.org/product" target="_blank">A VIDEO REEL</a></li>
                <li><a href="https://bitbucket.org/product" target="_blank">LOVE FOR OLD CAMERAS</a></li>
                <li><a href="https://bitbucket.org/product" target="_blank">DETAILED PROJECT PAGE</a></li>
                <li><a href="https://bitbucket.org/product" target="_blank">YOUTUBE VIDEO</a></li>
                <li><a href="https://bitbucket.org/product" target="_blank">ANOTHER EXTERNAL LINK</a></li>
                <li><a href="https://bitbucket.org/product" target="_blank">A LIGHTBOX IMAGE</a></li>
                <li><a href="https://bitbucket.org/product" target="_blank">THE DARK TEMPEST 8</a></li>
              </ul>
            </li>
            <li class="inicials"><a href="#home"><img src="Images/hero/jd.png" alt="JD"></a></li>
            <li><a href="#blog">My Blog</a></li>
    				<li><a href="#contactme">Contact Me</a></li>
    			</ul>
        </div>
  		</header>

  		<section class="hero" id="home">
  			<div class="center">
          <div class="line">
  	  			<h1 class="page-title">Retro</h1> 
            <img src="Images/line.png" alt="line">
          <div>
            <img id="old" src="Images/hero/old-style.png" alt="old">
          	<h3 class="below-retro">Old style portfolio</h3>
          </div>  
          </div>
	  			<div class="photo">
            <div class="slider-box">
              <div class="slider">
                <div>
    	  				 <img src="Images/hero/gray-hero.jpg" alt="Photo">
                </div>
                <div>
                 <img src="Images/hero/gray-hero.jpg" alt="Photo">
                </div>
              </div>
            <div>
            <h3>KODAK CAMERA</h3>
            </div>
            </div>
	  				<h3 class="below-photo">"Hello, I Am Ignas Kvederas,</h3>
	  				<h3 class="below-photo2">Welcome To Retro, My Wonderful Theme!"</h3>
	  			</div>
  			</div>
  		</section>

  		<section class="second-page" id="aboutme">
  			<div class="center">
  				<div>
            <div class="line">
    					<h2>About me</h2>
              <img src="Images/line.png" alt="line">
    					<p class="subtitle">I am web developer from Vilnius</p>
            </div>
  				</div>
  				<div class="me">
  					<div class="col">
  						<img src="Images/about1.png" alt="TV">
  						<h4>MOVIES</h4>
  						<h5>WHAT I WATCH</h5>
  						<p>Clutter is honest! I do believe that has to the users selfexpression.
                Care. Things which is a projector operating system such formats
                are deep and profound and meaningful. It's the traditions of data.
                Good design should be different levels. There's no rational alternative.
                That's come to complexity.
              </p>
  					</div>
  					<div class="col">
  						<img src="Images/about2.png" alt="disc">
  						<h4>MUSIC</h4>
  						<h5>WHAT I LISTEN</h5>
  						<p>Good design. The content, you'll love powerpoint should be different.
                 It's about design is that part? Good design is making something looks. And aesthetic. And colour defines your interface. That cannot be overtly different. That has to chance. And restrained, low resolution, but you get rid of materials.
              </p>
  					</div>
  					<div class="col">
  						<img src="Images/about3.png" alt="shirt">
  						<h4>CLOTHES</h4>
  						<h5>WHAT I WEAR</h5>
  						<p>Very honest approach and product. The other product that changes
                 function like the absence of function. And frustrating that products that it's the functional imperative, we kept going and frustrating that we are about bringing order to be better and less complicated to understand.
              </p>
  					</div>
  					<div class="col">
  						<img src="Images/about4.png" alt="food">
  						<h4>FOOD</h4>
  						<h5>WHAT I EAT</h5>
  						<p>Simplicity. Its context. From so many different levels. There's 
                real simplicity is. Designing and place of the objects we're surrounded by seem trivial. There's no rational alternative. I think that's a very complicated problems without letting people have a very easy to mean so much that acknowledges.
              </p>
  					</div>
  				</div>
  			</div>	
  		</section>

  		<section class="third-page" id="portfolio">
  			<div class="center">	
          <div class="line">
  					<h2>Portfolio</h2>
            <img src="Images/line.png" alt="line">
  					<p class="subtitle">Simplicity is the ultimate sophisticalion</p>
          </div>
          <div class="filter">
            <p class="filter-buttons">Filter by</p>
            <button class="design" type="button">design</button>
            <button class="logo" type="button">logo</button>
            <button class="photography" type="button">photography</button>
            <button class="poster" type="button">poster</button>
            <button class="resources" type="button">resources</button>
            <button class="retro" type="button">retro</button>
            <button class="t-shirts" type="button">t-shirts</button>
            <button class="videos" type="button">videos</button>
          </div>
			    <div class="project">
          	<div class="project-box design">
              <div class="gray-box">
                <a href="#">
                  <img src="Images/blog.jpg" alt="blank">
                </a>
              </div>
    					<a href="https://bitbucket.org/product" target="_blank">
                <img src="Images/portfolio/icon1.png" alt="picture">
    					 <h5>GALLERIES ARE FUNNY</h5>
              </a>
    				</div>
    				<div class="project-box logo">
              <div class="gray-box">
                <a href="#">
                  <img src="Images/blog.jpg" alt="blank">
                </a>
              </div>
              <a href="https://bitbucket.org/product" target="_blank">
                <img src="Images/portfolio/icon2.png" alt="TV">
    					 <h5>A VIDEO REEL</h5>
              </a>
    				</div>
    				<div class="project-box photography">
              <div class="gray-box">
                <a href="#">
                  <img src="Images/blog.jpg" alt="blank">
                </a>
              </div>
              <a href="https://bitbucket.org/product" target="_blank">
                <img src="Images/portfolio/icon3.png" alt="speaker">
    					 <h5>LOVE FOR OLD CAMERAS</h5>
              </a>
    				</div>
    				<div class="project-box poster">
              <div class="gray-box">
                <a href="#">
                  <img src="Images/blog.jpg" alt="blank">
                </a>
              </div>
              <a href="https://bitbucket.org/product" target="_blank">
                <img src="Images/portfolio/icon4.png" alt="book">
    					 <h5>DETAILED PROJECT PAGE</h5>
              </a>
    				</div>
            <div class="project-box resources">
              <div class="gray-box">
                <a href="#">
                  <img src="Images/blog.jpg" alt="blank">
                </a>
              </div>
              <a href="https://bitbucket.org/product" target="_blank">
                <img src="Images/portfolio/icon2.png" alt="tv">
                <h5>YOUTUBE VIDEO</h5>
              </a>
            </div>
            <div class="project-box retro">
              <div class="gray-box">
                <a href="#">
                  <img src="Images/blog.jpg" alt="blank">
                </a>
              </div>
              <a href="https://bitbucket.org/product" target="_blank">
                <img src="Images/portfolio/icon6.png" alt="clip">
                <h5>ANOTHER EXTERNAL LINK</h5>
              </a>
            </div>
            <div class="project-box t-shirts">
              <div class="gray-box">
                <a href="#">
                  <img src="Images/blog.jpg" alt="blank">
                </a>
              </div>
              <a href="https://bitbucket.org/product" target="_blank">
                <img src="Images/portfolio/icon1.png" alt="picture">
                <h5>A LIGHTBOX IMAGE</h5>
              </a>
            </div>
            <div class="project-box videos">
              <div class="gray-box">
                <a href="#">
                  <img src="Images/blog.jpg" alt="blank">
                </a>
              </div>
              <a href="https://bitbucket.org/product" target="_blank">
                <img src="Images/portfolio/icon3.png" alt="speaker">
                <h5>THE DARK TEMPEST</h5>
              </a>
            </div>
          </div>
          <div class="browse-all">
            <a href="#portfolio">BROWSE ALL</a>
          </div>
   			</div>
  		</section>

      <section class="blog" id="blog">
        <div class="center">
          <div class="page-top-blog line">
            <h2>MY BLOG</h2>
            <img src="Images/line.png" alt="line">
            <p class="subtitle">NEWS FROM MY HOUSE</p>
          </div>
          <div class="content">
            <div class="blog-box">
              <img src="Images/blog.jpg" alt="blank">
              <div class="post-text">
                <h6>The Magic Wheel</h6>
                <div class="date-response">
                  <p class="date-admin">JULY 28, 2014 // ADMIN // FUN, TRAVEL, TRENDS</p>
                  <p>1 RESPONSE</p>
                </div>
                <p class="post">It’s not it to understand them in the objects we’re 
                  playing. Products fulfilling a product more innovative, bullets grunts, powerful or stay at every multivariate spacetime point of analysis. People have enormous serial correlation. Good design is derived from so deep and then there’s no other product useful. 
                  <a href="#">(MORE…)</a>
                </p>
              </div>
            </div>
            <div class="blog-box">
              <img src="Images/blog.jpg" alt="blank">
              <div class="post-text">
                <h6>The lonely road</h6>
                <div class="date-response">
                  <p class="date-admin">JULY 28, 2014 // ADMIN // FUN, TRAVEL, TRENDS, WEB DESIGN</p>
                  <p>3 RESPONSES</p>
                </div>
                <p class="post">If you’ve studied design at all, you’ve probably 
                  encountered Lorem Ipsum placeholder text at some point. Anywhere there is text, but the meaning of that text isn’t particularly important, you might see Lorem Ipsum. 
                  <a href="#">(MORE…)</a>
                </p>
              </div>
            </div>
            <div class="blog-box">
              <img src="Images/blog.jpg" alt="blank">
              <div class="post-text">
                <h6>I love gasoline smell</h6>
                <div class="date-response">
                  <p class="date-admin">JULY 28, 2014 // ADMIN // FUN, TRENDS, WEB DESIGN</p>
                  <p>NO RESPONSES</p>
                </div>
                <p class="post">Many of the content, and often about their 
                  precious turfpossessed selves and principles are shortcomings of seeing and colors to cover up a paucity of design. Zero out your interface. I think that are deep and profound indeed, these tasks and showing. 
                  <a href="#">(MORE…)</a>
                </p>
              </div>
            </div>
          </div>
          <div class="browse-all">
            <a href="#blog">SHOW ALL POSTS</a>
          </div>
        </div>
      </section>

  		<section class="fourth-page" id="contactme">
  			<div class="center">
  				<div class="page-top">
            <div class="line">
    					<h2>Contact me</h2>
              <img src="Images/line.png" alt="line">
    					<p class="subtitle">I'l be glad to answer your questions</p>
            </div>
  				</div>
          <div class= "email-form">
    				<form class="contact-form">
          		<div class="input-row">
            			<input type="text" name="name" placeholder="Your Name*">
            			<input type="text" name="mail" placeholder="Your Email*">
          		</div>
              <div class="text-area">
            		<textarea name="message" rows="8" placeholder="Your Message*"></textarea>
            		<input class="btn btn-form" type="submit" name="submit" value="Send Message">
        		  </div>
            </form>
          </div>
  			</div>
  		</section>

      <footer class="fourth-page">
        <div class="center">
          <div class="below-form">
            <h3>I am social</h3>
          </div>
          <div class="social">
            <ul>
              <li><a href="#"><i class="fab fa-facebook-square"></i></a></li>
              <li><a href="#"><i class="fab fa-linkedin"></i></a></li>
              <li><a href="#"><i class="fas fa-envelope-square"></i></a></li>
              <li><a href="#"><i class="fas fa-phone-square"></i></a></li>     
            </ul>
          </div>
          <div class= "data">
            <p><?php echo date('l jS \of F Y h:i:s A'); ?></p>
          </div>
        </div>  
      </footer>

    <script type="text/javascript" src="jquery.min.js"></script>
    <script type="text/javascript" src="slick.min.js"></script>
    <script type="text/javascript" src="portfolio.js"></script>
    <script type="text/javascript" src="scripts.js"></script>
        
	</body>

</html>


